import time, hmac, hashlib, json, logging, boto3, os

SLACK_REQUEST_HEADER = 'X-Slack-Request-Timestamp'
SLACK_SIG_HEADER = 'X-Slack-Signature'
SLACK_SIGNING_KEY = os.environ['SLACK_KEY']
AWS_SQS_QUEUE = os.environ['AWS_SQS_QUEUE']

root_logger = logging.getLogger()

if root_logger.handlers:
    for handler in root_logger.handlers:
        root_logger.removeHandler(handler)
        
logging.basicConfig(format='[%(levelname)s] %(asctime)s - %(message)s', level=logging.DEBUG)

def lambda_handler(event: dict, context: dict) -> str:
    """ 
    Handle incoming HTTP request from Slack
    """
    root_logger.debug(event)
    event_headers = event['headers']
    event_body = event['body']
    response_body = ""
    
    if verified_event(event_headers, event_body):
        root_logger.info("Request is verified")
        if "challenge" in event_body:
            root_logger.info("Challenge event, responding with challenge")
            
            challenge_event = convert_to_dict(event_body)
            
            response_body = challenge_event["challenge"]
            
        else:
            root_logger.info("Writing message to queue")
            queue_event(event_body)
    else:
        root_logger.error("Event is not verified!")
    
    return json_response(response_body)
        

def convert_to_dict(event_body: str):
    """
    Convert string into JSON dict
    """
    
    return json.loads(event_body)
    
def verified_event(event_headers: dict, event_body: dict) -> bool:
    """
    Verifies that the request is a Slack event
    """
    
    timestamp = event_headers[SLACK_REQUEST_HEADER]
    signature = event_headers[SLACK_SIG_HEADER]
    
    if abs(time.time() - int(timestamp)) > 60 * 5:
        return False

    basestring = f"v0:{timestamp}:{event_body}".encode('utf-8')
    app_sig = get_app_signature(basestring)
    
    verified = hmac.compare_digest(signature, app_sig)
    
    if not verified:
        root_logger.warn(f"Verification failed. App signature {app_sig}")
        
    return verified


def get_app_signature(basestring: str) -> str:
    """
    Computes the app's hex digst based on the base string 
    from request timestamp, and request body
    """
    
    bytes_signing_secret = bytes(SLACK_SIGNING_KEY, 'utf-8')
    
    get_app_signature = hmac.new(bytes_signing_secret, basestring, hashlib.sha256).hexdigest()
        
    return 'v0=' + get_app_signature

def queue_event(event_body: dict):
    """
    Queue event to AWS SQS for later processing
    """
    
    event_json = json.loads(event_body)
    root_logger.info(event_json)
    
    root_logger.info(event_body)
    event_channel = event_json['event']['channel']
    
    sqs = boto3.resource('sqs')
    
    queue = sqs.get_queue_by_name(QueueName=AWS_SQS_QUEUE)
    
    response = queue.send_message(MessageBody=json.dumps(event_json), 
        MessageGroupId=event_channel)
    
    root_logger.info(response)

def json_response(body: str):
    """
    Returns the HTTP response to API gateway
    """
    
    response = {
        "statusCode": 200,
        "headers": {},
        "body": body,
        "isBase64Encoded": False
    }
    
    return response