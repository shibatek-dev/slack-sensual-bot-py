# -*- coding: utf-8 -*-

import urllib.request, json, datetime, os

CITY_ID = 6173331
API_KEY = os.environ['WEATHER_API_KEY']
SLACK_WEBHOOK = os.environ['SLACK_WEBHOOK']

def lambda_handler(event: dict, context: dict) -> str:
    """
    AWS Lambda handler
    """
    
    weather_data = get_current_weather()
    messages = build_messages(weather_data)
    
    send_slack_message(messages)

def get_current_weather() -> dict:
    """"
    Makes API request and returns current weather in JSON
    """
    
    weather_data = {}
    
    with urllib.request.urlopen(f"https://api.openweathermap.org/data/2.5/weather?id={CITY_ID}&units=metric&appid={API_KEY}") as url:
        weather_data = json.loads(url.read().decode())
    
    return weather_data
 
 
def build_messages(weather_data: dict) -> dict:
    """
    Builds the message for the Slack post
    """
     
    weather_conditions_list = weather_data['weather']
    temperature = weather_data['main']
    
    weather_conditions = weather_conditions_list[0]
    
    condition_message = f"\u2022 Condition: *{weather_conditions['main']}, {weather_conditions['description']}*"
    temp_message = f"\n \u2022 Temperature: *{temperature['temp']}C* (feels like *{temperature['feels_like']}C*) \n"
    high_low_message = f"\u2022 High: {temperature['temp_max']}C \n \u2022 Low: {temperature['temp_min']}C"
     
    weather_message = condition_message + temp_message + high_low_message
     
    morning_message = "Remember that I need lots of love. As part of your service to me, I require walks. I am a generous lord and will tell you what the weather in Vancouver is like right now."
             
    evening_message = "You are almost late for your shift! To make your commute easier to start your Honoring Service, this is the current weather"
     
    icon = f"http://openweathermap.org/img/wn/{weather_conditions['icon']}@2x.png"
     
    messages = {
        "greeting": morning_message, 
        "weather": weather_message,
        "images" : icon
        }
     
    return messages
     
def send_slack_message(messages):
    """
    Post Message to Slack channel
    """

    data = {
        "blocks": [
            {
                "type": "section",
                "text": {
                    "type": "mrkdwn",
                    "text": "Hi Minions! :wave:"
                }
            },
            {
                "type": "section",
                "text": {
                    "type": "mrkdwn",
                    "text": messages['greeting']
                }
            },
            {
                "type": "image",
                "image_url": messages['images'],
                "alt_text": "weather"
            },
            {
                "type": "section",
                "text": {
                    "type": "mrkdwn",
                    "text": messages['weather']
                }
            }
        ]
    }
    
    params = json.dumps(data).encode('utf8')
    req = urllib.request.Request(SLACK_WEBHOOK, data=params, headers={'content-type': 'application/json'})
    
    urllib.request.urlopen(req)
